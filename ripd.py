import os
import socket

import sys
import threading

import bottle

from commands import Commands
from config import BUFFER_SIZE
import logging

from task_manager import TaskManager
from utils import get_server_args

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

server_args = get_server_args()

task_manager = TaskManager()

tasks_loaded = task_manager.load_tasks(server_args.templates_paths, server_args.tasks_paths)

if not tasks_loaded:
    logging.error('Could not load tasks')
    sys.exit(-1)

task_manager.list_tasks()


@bottle.post('/rip/<task_name>/<job_name>')
def execute_rip_task(task_name, job_name):
    token = bottle.request.POST.get('token')
    threaded = bottle.request.POST.get('threaded', False)

    if token is None:
        print('[E] Unknown token: %s' % token)
        return {'status': 'fail', 'error': 'Unknown token: %s' % token}

    tasks = task_manager.get_tasks()

    if task_name not in tasks or job_name not in tasks[task_name].get('jobs', {}):
        print('[E] No suitable configuration for task \'%s\', job \'%s\' found' % (task_name, job_name))
        return {'status': 'fail', 'error': 'No suitable configuration for task \'%s\', job \'%s\' found'
                                           % (task_name, job_name)}

    job_description = tasks.get(task_name, {}).get('jobs', {}).get(job_name)

    if job_description['token'] != token:
        print('[E] Unknown token: %s' % token)
        return {'status': 'fail', 'error': 'Unknown token: %s' % token}

    if threaded:
        # don't block on this response
        actor = threading.Thread(target=TaskManager.execute_job, args=[task_name,
                                                                       job_name,
                                                                       job_description['working_dir'],
                                                                       job_description['script']])
        actor.start()
    else:
        TaskManager.execute_job(task_name, job_name, job_description['working_dir'], job_description['script'])

    return {'status': 'ok'}


def create_server_socket():
    if os.path.exists(server_args.socket_path):
        # make sure socket does not exist
        try:
            os.unlink(server_args.socket_path)
        except OSError:
            if os.path.exists(server_args.socket_path):
                raise Exception('Already running (socket file already exists: "%s")' % server_args.socket_path)

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(server_args.socket_path)

    return sock


def start_web_server():
    web_server_actor = threading.Thread(target=bottle.run, kwargs={
        'host': server_args.host,
        'port': server_args.port,
        'quiet': True
    })

    web_server_actor.start()


def main():
    global server_args

    start_web_server()

    sock = create_server_socket()

    sock.listen(1)

    while True:
        connection, client_address = sock.accept()

        try:
            data = connection.recv(BUFFER_SIZE).decode('utf-8')
            data = data.strip().lower()

            if data.startswith(Commands.config_reload):
                server_args = get_server_args()

                logging.info('Config has been reloaded')
            elif data.startswith(Commands.tasks_reload):
                tasks_reloaded = task_manager.load_tasks(server_args.templates_paths, server_args.tasks_paths)

                if tasks_reloaded:
                    logging.info('Tasks have been reloaded')
                    task_manager.list_tasks()
                else:
                    logging.info('Tasks have NOT been reloaded')
            else:
                logging.warning('Unsupported command: "%s"' % data)
        finally:
            connection.close()


if __name__ == '__main__':
    main()
