from collections import OrderedDict

import sys

import yaml

from utils import get_directory_filenames, merge_dicts
import logging


logging.basicConfig(level=logging.INFO, stream=sys.stdout)


class TaskTemplatesParser:
    TEMPLATE_EXTENSIONS = ['yml', 'yaml']

    TEMPLATE_GLOBAL_OPTIONAL_KEYWORDS = ['token', 'working_dir']
    TEMPLATE_JOB_REQUIRED_KEYWORDS = ['token', 'working_dir', 'script']

    def __init__(self):
        self._templates = OrderedDict()

    @staticmethod
    def templatize_content(content_text, variables):
        for var in variables:
            content_text = content_text\
                .replace('$%s' % var)\
                .replace('${%s}' % var)

        return content_text

    @staticmethod
    def get_template_content(template_path):
        with open(template_path) as template_file:
            template_content = template_file.read()

            try:
                return yaml.load(template_content)
            except yaml.YAMLError as exc:
                logging.error(exc)
                return None

    @staticmethod
    def normalize_template_content(template_path, content):
        template_name = content.get('name')

        if template_name is None:
            logging.error('"name" field of template is not specified: template = %s' % template_path)
            return None

        template_jobs = content.get('jobs')

        if template_jobs is None:
            logging.error('"jobs" field of template is not specified: template = %s' % template_path)
            return None

        if len(template_jobs.items()) < 1:
            logging.error('No jobs specified in the template: template = %s' % template_path)
            return None

        result = {
            'name': template_name,
            'jobs': {}
        }
        global_values = {}

        for kw in TaskTemplatesParser.TEMPLATE_GLOBAL_OPTIONAL_KEYWORDS:
            val = content.get(kw)

            if val is not None:
                global_values[kw] = val

        for job_name, job in template_jobs.items():
            if job_name in result['jobs'].keys():
                logging.error('Duplicate job name: job_name = %s, template = %s' % (job_name, template_path))
                return None

            result['jobs'][job_name] = {}

            for kw in TaskTemplatesParser.TEMPLATE_JOB_REQUIRED_KEYWORDS:
                local_val = job.get(kw)
                global_val = global_values.get(kw)

                if local_val is None and global_val is None:
                    logging.error('"%s" field of the job is not specified: job_name = %s, template = %s' % (kw, job_name, template_path))
                    return None

                if local_val is None:
                    final_val = global_val
                elif global_val is None:
                    final_val = local_val
                else:
                    if isinstance(global_val, str) and isinstance(local_val, str):
                        final_val = local_val
                    else:
                        if isinstance(global_val, dict) and isinstance(local_val, dict):
                            final_val = merge_dicts(global_val, local_val)
                        else:
                            logging.error('Could not merge two field values: template_name = %s, job_name = %s, '
                                          'field = %s' % (template_name, job_name, kw))
                            return None

                result['jobs'][job_name][kw] = final_val

        # sort jobs by name
        result['jobs'] = OrderedDict(sorted(result['jobs'].items()))

        return result

    def get_templates(self):
        return self._templates

    def get_template(self, name):
        return self._templates.get(name)

    def load_templates(self, templates_paths):
        # TODO: set default template paths
        templates = {}
        templates_loaded = True

        try:
            for templates_path in templates_paths:
                template_files = get_directory_filenames(templates_path, TaskTemplatesParser.TEMPLATE_EXTENSIONS)

                for template_path in template_files:
                    content = TaskTemplatesParser.get_template_content(template_path)

                    if content is None:
                        logging.error('Template content is not defined: template = %s' % template_path)
                        templates_loaded = False
                        break

                    normalized_content = TaskTemplatesParser.normalize_template_content(template_path, content)

                    if normalized_content is None:
                        logging.error('Template content can not be normalized: template = %s' % template_path)
                        templates_loaded = False
                        break

                    templates[normalized_content['name']] = normalized_content

                if not templates_loaded:
                    break

        except Exception as e:
            logging.error('Templates have not been loaded')
            templates_loaded = False

        if templates_loaded:
            # sort templates by name
            templates = OrderedDict(sorted(templates.items()))

            self._templates = templates

        return templates_loaded

    def list_templates(self):
        logging.info('=' * 80)
        logging.info('Loaded %s task templates:\n' % len(self._templates.keys()))

        for template_name, template in self._templates.items():
            logging.info('* %s' % template_name)

            for job_name, job in template.get('jobs', {}).items():
                logging.info('  - %s' % job_name)

        logging.info('')
        logging.info('=' * 80)

