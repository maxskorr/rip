import logging

import sys

from copy import deepcopy

from task_manager.parsers.task_parser import TasksParser
from task_manager.parsers.template_parser import TaskTemplatesParser
from utils import merge_dicts
import subprocess

logging.basicConfig(level=logging.INFO, stream=sys.stdout)


class TaskManager:
    def __init__(self):
        self._task_templates_parser = TaskTemplatesParser()
        self._task_parser = TasksParser()
        self._tasks = {}

    @staticmethod
    def templatize_str(str_val: str, variables: dict):
        for var_name, var_val in variables.items():
            str_val = str_val.replace('$%s' % var_name, var_val)
            str_val = str_val.replace('${%s}' % var_name, var_val)

        return str_val

    @staticmethod
    def templatize_list(list_val: list, variables: dict, create_new=True):
        result = list_val

        if create_new:
            result = deepcopy(list_val)

        for i, v in enumerate(result):
            if isinstance(v, str):
                result[i] = TaskManager.templatize_str(v, variables)

            if isinstance(v, list):
                result[i] = TaskManager.templatize_list(v, variables, False)

            if isinstance(v, dict):
                result[i] = TaskManager.templatize_dict(v, variables, False)

        return result

    @staticmethod
    def templatize_dict(dict_val: dict, variables: dict, create_new=True):
        result = dict_val

        if create_new:
            result = deepcopy(dict_val)

        for k, v in result.items():
            if isinstance(v, str):
                result[k] = TaskManager.templatize_str(v, variables)

            if isinstance(v, list):
                result[k] = TaskManager.templatize_list(v, variables, False)

            if isinstance(v, dict):
                result[k] = TaskManager.templatize_dict(v, variables, False)

        return result

    @staticmethod
    def build_task(template, task):
        built_task = deepcopy(template)

        built_task['name'] = task.get('name')
        built_task['template'] = task.get('template')

        task_jobs = built_task.get('jobs', {})

        vars_global = task.get('variables', {})

        for job_name, job in task_jobs.items():
            vars_local = task.get('jobs', {}).get(job_name, {}).get('variables', {})

            # merge variables: locals override globals
            vars_all = merge_dicts(vars_global, vars_local)

            # update the job with the templatized one
            task_jobs[job_name] = TaskManager.templatize_dict(job, vars_all)

        return built_task

    def build_tasks(self):
        built_tasks = {}

        templates = self._task_templates_parser.get_templates()
        tasks = self._task_parser.get_tasks()

        for task_name, task in tasks.items():
            template_name = task.get('template')

            if template_name not in templates.keys():
                logging.error('Template not found: template_name = %s, task_name = %s' % (template_name, task_name))
                return None

            template = templates[template_name]

            built_tasks[task_name] = TaskManager.build_task(template, task)

        return built_tasks

    def load_tasks(self, templates_paths: list, tasks_paths: list):
        templates_loaded = self._task_templates_parser.load_templates(templates_paths)

        if not templates_loaded:
            logging.error('Could not load task templates')
            return False

        self._task_templates_parser.list_templates()

        variables_loaded = self._task_parser.load_tasks(tasks_paths)

        if not variables_loaded:
            logging.error('Could not load task variables')
            return False

        self._task_parser.list_tasks()

        tasks = self.build_tasks()

        if tasks is None:
            logging.error('Could not load tasks')
            return False

        self._tasks = tasks

        logging.info('Tasks have been loaded')

        return True

    def get_tasks(self):
        return self._tasks

    def list_tasks(self):
        logging.info('=' * 80)
        logging.info('Built %s tasks:\n' % len(self._tasks.keys()))

        for task_name, task in self._tasks.items():
            logging.info('* %s' % task_name)

            for job_name, job in task.get('jobs', {}).items():
                logging.info('  - %s' % job_name)

        logging.info('')
        logging.info('=' * 80)

    @staticmethod
    def execute_job(task_name, job_name, working_dir, script):
        logging.info('>> Running job "%s" of task "%s"' % (job_name, task_name))

        for line in script:
            subprocess.call(line, shell=True, cwd=working_dir)
