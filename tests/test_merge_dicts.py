from utils import merge_dicts

dict1 = {1: {"a": "A"}, 2: {"b": {"B": "B"}}}
dict2 = {2: {"c": "C", "b": {'X': "X"}}, 3: {"d": "D"}}

dict3 = merge_dicts(dict1, dict2)

print(id(dict3), dict3)

dict4 = merge_dicts(dict1, {}, False)

print(id(dict4), dict4)

print(id(dict1), dict1)
